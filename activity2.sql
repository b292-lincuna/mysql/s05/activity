
USE record_db;


SELECT	song_name AS "Song Name"
FROM	songs
WHERE	album_id = 9 AND genre LIKE "%Funk%";


/*##############################################*/
-- no result because there is no song that has length is less than 3 mins
SELECT	song_name   AS "Song Name", 
		album_title AS "Album Title" ,
		name        AS "Artist Name"
FROM	songs
JOIN	albums  ON songs.album_id = albums.id
JOIN	artists ON albums.artist_id = artists.id
WHERE	length < 300 AND YEAR(date_released) = 2015;


/*##############################################*/
-- no result because there is no name that ends with o
SELECT	artists.name       AS "Artist Name", 
		albums.album_title AS "Album Title" 
FROM	artists 
JOIN	albums ON artists.id = albums.artist_id
WHERE	artists.name LIKE "%o" AND YEAR(date_released) < 2013;


/*##############################################*/
SELECT	song_name AS "Song Name"
FROM	songs
JOIN	albums ON songs.album_id = albums.id
WHERE	(length BETWEEN 300 AND 400) AND artist_id = 1;


/*##############################################*/
SELECT	album_title AS "Album Title" ,
		date_released AS "Date Released"
FROM	albums 
JOIN	artists ON albums.artist_id = artists.id
WHERE	album_title LIKE "%born%" AND artists.name LIKE "J%";


/*##############################################*/
SELECT	song_name AS "Song Name"
FROM	songs 
JOIN	albums  ON songs.album_id=albums.id
JOIN	artists ON albums.artist_id=artists.id
WHERE	artists.name LIKE "b%";


/*##############################################*/
SELECT song_name AS "Song Name" FROM songs WHERE length > 400;