-- MySQL S5 Activity:

/*##########################################################*/
-- 1. Return the customerName of the customers who are from the Philippines
SELECT	customerName AS "Customer Name" 
FROM	customers 
WHERE	country = "Philippines";


/*##########################################################*/
-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT	contactLastName, contactFirstName 
FROM	customers 
WHERE	customerName = "La Rochelle Gifts";


/*##########################################################*/
-- 3. Return the product name and MSRP of the product named "The Titanic"
SELECT	productName AS "Product Name" , MSRP 
FROM	products 
WHERE	productName = "The Titanic";


/*##########################################################*/
-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT	firstName AS "First Name", 
		lastName  AS "Last Name" 
FROM	employees
WHERE	email = "jfirrelli@classicmodelcars.com";


/*##########################################################*/
-- 5. Return the names of customers who have no registered state
SELECT	customerName AS "Customer Name" 
FROM	customers 
WHERE	state IS null;


/*##########################################################*/
-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT	firstName AS "First Name", 
		lastName  AS "Last Name", Email 
FROM	employees 
WHERE	lastName = "Patterson" AND firstName = "Steve";


/*##########################################################*/
-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT	customerName AS "Customer Name", 
		Country,
		format(creditLimit,'standard') AS "Credit Limit" 
FROM	customers
WHERE	country != "USA" AND creditLimit > 3000;


/*##########################################################*/
-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT	customerNumber AS "Customer Number"
FROM	orders 
WHERE	comments LIKE "%DHL%";


/*##########################################################*/
-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT	productLine AS "Product Lines" 
FROM	productLines 
WHERE	textDescription LIKE "%state of the art%";


/*##########################################################*/
-- 10. Return the countries of customers without duplication
-- SELECT DISTINCT country FROM customers ORDER BY country;
SELECT DISTINCT country FROM customers;


/*##########################################################*/
-- 11. Return the statuses of orders without duplication
-- SELECT DISTINCT status FROM orders ORDER BY status;
SELECT DISTINCT status FROM orders;


/*##########################################################*/
-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT	customerName AS "Customer Name", Country
FROM	customers
WHERE	country IN ("USA","France","Canada")
ORDER BY country, customerName;


/*##########################################################*/
-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo

-- SELECT CONCAT_WS(' ', employees.firstName, employees.lastName) AS "Full Name"
SELECT	firstName    AS "First Name",
		lastName     AS "Last Name",
		offices.city AS "Office City" 
FROM	employees 
JOIN	offices ON employees.officeCode = offices.officeCode 
WHERE	offices.city = "Tokyo";


/*##########################################################*/
-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"

-- SELECT customerName as "Customer Name" FROM customers WHERE salesRepEmployeeNumber=1166; 
/*OR*/

SELECT	customerName AS "Customer Name"
FROM	customers 
WHERE	salesRepEmployeeNumber = (
	SELECT	employeeNumber 
	FROM	employees 
	WHERE	lastName = "Thompson" AND firstName = "Leslie"
);


/*##########################################################*/
-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
-- SELECT customerNumber FROM customers WHERE customerName = "Baane Mini Imports";

SELECT	productName AS "Product Name", customerName AS "Customer Name" 
FROM	customers 
JOIN	orders       ON customers.customerNumber =  orders.customerNumber 
JOIN	orderDetails ON orders.orderNumber = orderDetails.orderNumber
JOIN	products     ON orderDetails.productCode = products.productCode
WHERE	customers.customerNumber = 121
-- WHERE customerName = "Baane Mini Imports";
-- ORDER BY productName;


/*##########################################################*/
-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT 	employees.firstName    AS "Employee First Name", 
		employees.lastName     AS "Employee Last Name",
		customers.customerName AS "Customer Name",
		offices.country        AS "Office's Country"
FROM 	customers 
JOIN 	employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN 	offices   ON employees.officeCode = offices.officeCode
WHERE 	customers.country = offices.country;


/*##########################################################*/
-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT 	productName     AS "Product Name", 
		quantityInStock AS "Qnty. in Stock" 
FROM 	products
WHERE	productLine = "planes" AND quantityInStock < 1000;


/*##########################################################*/
-- 18. Show the customer's name with a phone number containing "+81".
SELECT	customerName AS "Customer Name", 
		phone        AS "Phone Number" 
FROM	customers 
WHERE	phone LIKE "%+81%";
